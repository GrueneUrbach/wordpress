<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}


function theme_slug_default_featured_image( $dfi_thumbnail_id, $post_id ) {
	$post = get_post( $post_id );

	// Show default featured image only on blog posts
	if ( 'post' === $post->post_type ) {
		return $dfi_thumbnail_id;
	}

	// Return invalid Image ID
	return 0;
}
add_filter( 'dfi_thumbnail_id', 'theme_slug_default_featured_image', 10, 2 );


function my_login_logo() { ?>
    <style type="text/css">
        body.login { background: #559448; }
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
		background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function child_theme_head_script() { ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css" integrity="sha256-gsmEoJAws/Kd3CjuOQzLie5Q3yshhvmo7YNtBG7aaEY=" crossorigin="anonymous">
<?php }
add_action( 'wp_head', 'child_theme_head_script' );

/** hide social media buttons */
add_filter('kr8_contentsingle_share', '__return_false');
