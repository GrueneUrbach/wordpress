				</div>
			</section>
			<footer id="footer" role="contentinfo">
				<div class="inner wrap clearfix">
	                		
					<section class="sidebar cleafix">
						<ul>
							<?php dynamic_sidebar('fussleiste'); ?>
						</ul>	
					</section>
					<nav role="navigation" class="footer-menu">
    					<?php kr8_nav_footer(); ?>
	                </nav>
				<p id="social">
					<a href="https://gruene.social/@urbach" title="Mastodon" rel="me"><i class="fa fa-mastodon" aria-hidden="true"></i></a>
					<!--<a href="https://pixelfed.social/GrueneUrbach" title="Pixelfed" rel="me"><i class="fa fa-pixelfed" aria-hidden="true"></i></a>-->
					<!--<a href="https://t.me/GrueneUrbach" title="Telegram Kanal" rel="me"><i class="fa fa-telegram" aria-hidden="true"></i></a>-->
					<a href="https://twitter.com/GrueneUrbach" title="Twitter" rel="me"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a href="https://facebook.com/GrueneUrbach" title="Facebook" rel="me"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="https://instagram.com/GrueneUrbach" title="Instagram" rel="me"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				</p>
				</div> <!-- end #inner-footer -->
			</footer> 
			<p class="copyright">
Dieses Werk ist lizenziert unter einer <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.de">Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.
			</p>
		</div> 
		<p id="back-top"><a href="#header" title="Zum Seitenanfang springen"><span>↑</span></a></p>

		<?php wp_footer(); ?>

	</body>
</html>
